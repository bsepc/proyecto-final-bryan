-- MySQL dump 10.13  Distrib 8.0.23, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: mydb
-- ------------------------------------------------------
-- Server version	8.0.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `area_salud`
--

DROP TABLE IF EXISTS `area_salud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `area_salud` (
  `idarea_salud`  int NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idarea_salud`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

DROP TABLE IF EXISTS `distrito_salud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `distrito_salud` (
  `iddistrito_salud` int NOT NULL,
  `distrito_saludcol` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`iddistrito_salud`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;
--
-- Table structure for table `etnia`
DROP TABLE IF EXISTS `etnia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `etnia` (
  `idetnia` int NOT NULL,
  `etniacol` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idetnia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;
LOCK TABLES `etnia` WRITE;
/*!40000 ALTER TABLE `etnia` DISABLE KEYS */;
INSERT INTO `etnia` VALUES (1,'indigena'),(2,'no indigena'),(3,'no especifica');
/*!40000 ALTER TABLE `etnia` ENABLE KEYS */;
UNLOCK TABLES;


-- Table structure for table `evento`
DROP TABLE IF EXISTS `evento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `evento` (
  `idevento` int NOT NULL,
  `eventocol` varchar(1000) NOT NULL,
  `fecha_inicio` varchar (30) DEFAULT NULL,
  `fecha_fin` varchar(30) DEFAULT NULL,
  `objetivo` varchar(2000) NOT NULL,
  `part_program` int DEFAULT NULL,
  `part_asistido` int DEFAULT NULL,
  `horas` varchar(20) DEFAULT NULL,
  `idtipo_evento` int NOT NULL,
  `idmodalidad` int NOT NULL,
  PRIMARY KEY (`idevento`),
  KEY `fk_evento_tipo_evento1_idx` (`idtipo_evento`),
  KEY `fk_evento_modalidad1_idx` (`idmodalidad`),
  CONSTRAINT `fk_evento_modalidad1` FOREIGN KEY (`idmodalidad`) REFERENCES `modalidad` (`idmodalidad`),
  CONSTRAINT `fk_evento_tipo_evento1` FOREIGN KEY (`idtipo_evento`) REFERENCES `tipo_evento` (`idtipo_evento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--

--
-- Table structure for table `modalidad`
--

DROP TABLE IF EXISTS `modalidad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `modalidad` (
  `idmodalidad`  int NOT NULL,
  `modalidadcol` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idmodalidad`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `modalidad` WRITE;
/*!40000 ALTER TABLE `modalidad` DISABLE KEYS */;
INSERT INTO `modalidad` VALUES (1,'Crow Learnig'),(2,'b-learning'),(3,'Presencial');
/*!40000 ALTER TABLE `modalidad` ENABLE KEYS */;
UNLOCK TABLES;
-- Table structure for table `profesion`
--

DROP TABLE IF EXISTS `profesion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `profesion` (
  `idprofesion` int NOT NULL,
  `profesioncol` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idprofesion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `profesion` WRITE;
/*!40000 ALTER TABLE `profesion` DISABLE KEYS */;
INSERT INTO `profesion` VALUES (1,'	Médico y Cirujano'),(2,'	Licenciatura en Enfermería'),(3,'Licenciatura en Nutrición'),(4,'Licenciatura en psicología'),(5,'Licenciatura en Administración de empresas'),(6,'Licenciatura en Pedagogía'),(7,'Licenciatura en Trabajo Social'),(8,'Técnico en Enfermería'),(9,'Ingeniería en Sistemas'),(10,'Ingeniería Agroforestal'),(11,'Químico biólogo'),(12,'Abogado y Notario'),(13,'Odontología'),(14,'Auxiliares de enfermería'),(15,'Inspector de Saneamiento Ambiental'),(16,'Otras');
/*!40000 ALTER TABLE `profesion` ENABLE KEYS */;
UNLOCK TABLES;
--


--
-- Table structure for table `registro`
--

DROP TABLE IF EXISTS `registro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `registro` (
  `idregistro` int NOT NULL,
  `registrocol` varchar(45) DEFAULT NULL,
  `idusuario` int NOT NULL,
  `idevento` int NOT NULL,
  PRIMARY KEY (`idregistro`),
  KEY `fk_registro_usuario1_idx` (`idusuario`),
  KEY `fk_registro_evento1_idx` (`idevento`),
  CONSTRAINT `fk_registro_evento1` FOREIGN KEY (`idevento`) REFERENCES `evento` (`idevento`),
  CONSTRAINT `fk_registro_usuario1` FOREIGN KEY (`idusuario`) REFERENCES `usuario` (`idusuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;



--
-- Table structure for table `rol`
--

DROP TABLE IF EXISTS `rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rol` (
  `id_role` int NOT NULL,
  `nombrerol` varchar(24) DEFAULT NULL,
   PRIMARY KEY (`id_role`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rol`
--

LOCK TABLES `rol` WRITE;
/*!40000 ALTER TABLE `rol` DISABLE KEYS */;
/*!40000 ALTER TABLE `rol` ENABLE KEYS */;
INSERT INTO `rol` VALUES (1,'Dirección o coordinación de Distrito'),(2,'Enfermero/a de distrito'),(3,'Enfermero/a de programa'),(4,'Enfermero/a de territorio'),(5,'Auxiliar de enfermería sin especificación de cargo'),(6,'Técnico en salud rural'),(7,'Dirección de área de salud'),(8,'Enfermera/o de área de salud'),(9,'Gerente de Provisión de Servicio'),(10, 'Coordinador de programas sin especificación'),(11,'Otros');
UNLOCK TABLES;

--DROP TABLE IF EXISTS `cargo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;

-- Table structure for table `sexo`
--

DROP TABLE IF EXISTS `sexo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sexo` (
  `idsexo` int NOT NULL,
  `sexocol` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idsexo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `sexo` WRITE;
/*!40000 ALTER TABLE `sexo` DISABLE KEYS */;
INSERT INTO `sexo` VALUES (1,'Mujer'),(2,'Hombre'),(3,'No especifica');
/*!40000 ALTER TABLE `sexo` ENABLE KEYS */;
UNLOCK TABLES;




DROP TABLE IF EXISTS `tipo_evento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tipo_evento` (
  `idtipo_evento` int NOT NULL,
  `tipo_eventocol` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idtipo_evento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `tipo_evento` WRITE;
/*!40000 ALTER TABLE `tipo_evento` DISABLE KEYS */;
INSERT INTO `tipo_evento` VALUES (1,'Taller'),(2,'Seminario'),(3,'Foro'),(4,'Conferencia'),(5,'Diplomado'),(6,'Curso Corto');
/*!40000 ALTER TABLE `tipo_evento` ENABLE KEYS */;
UNLOCK TABLES;

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuario` (
  `idusuario` int (11) NOT NULL,
  `cui` varchar(45) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `apellidos` varchar(45) DEFAULT NULL,
  `id_distritoSalud` int NOT NULL,
  `id_area_salud` int NOT NULL,
  `id_profesion` int NOT NULL,
  /*`id_cargo` int NOT NULL,*/
  `id_sexo` int NOT NULL,
  `id_etnia` int NOT NULL,
  PRIMARY KEY (`idusuario`),
  KEY `fk_usuario_distrito_salud1_idx` (`id_distritoSalud`),
  KEY `fk_usuario_area_salud1_idx` (`id_area_salud`),
  KEY `fk_usuario_profesion1_idx` (`id_profesion`),  
 /* KEY `fk_usuario_cargo1_idx` (`id_cargo`),*/
  KEY `fk_usuario_sexo1_idx` (`id_sexo`),
  KEY `fk_usuario_etnia1_idx` (`id_etnia`),
  CONSTRAINT `fk_usuario_distrito_salud1` FOREIGN KEY (`id_distritoSalud`) REFERENCES `distrito_salud` (`iddistrito_salud`),
  CONSTRAINT `fk_usuario_area_salud1` FOREIGN KEY (`id_area_salud`) REFERENCES `area_salud` (`idarea_salud`),
  CONSTRAINT `fk_usuario_etnia1` FOREIGN KEY (`id_etnia`) REFERENCES `etnia` (`idetnia`),
  CONSTRAINT `fk_usuario_profesion1` FOREIGN KEY (`id_profesion`) REFERENCES `profesion` (`idprofesion`),
 /* CONSTRAINT `fk_usuario_cargo1` FOREIGN KEY (`id_cargo`) REFERENCES `cargo` (`idcargo`),*/
  CONSTRAINT `fk_usuario_sexo1` FOREIGN KEY (`id_sexo`) REFERENCES `sexo` (`idsexo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-01-07 21:02:05
