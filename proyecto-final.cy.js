describe('empty spec', () => {

  Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
  })

  it('passes', () => {
    cy.visit('http://localhost:70/consultas.php')
    cy.get('[name="cui"]').type('2862860680706')
    cy.get('[name=Submit]').click()
  })
})